<?php

namespace Drupal\status_dashboard;

/**
 * The interface for the Collector service.
 */
interface CollectorInterface {

  /**
   * Collects the updates info from the client site.
   *
   * @param string $url
   *   The client site url.
   * @param string $secrete
   *   The secrete phrase to connect.
   */
  public function collect(string $url, string $secrete);

}
