<?php

namespace Drupal\status_dashboard\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Project Updates field handler.
 *
 * @ViewsField("status_dashboard_project_updates")
 */
class ProjectUpdates extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $updates_list = [];
    $entity = $this->getEntity($values);
    if (!$entity) {
      return $updates_list;
    }

    if ($entity->hasSecurityUpdates()) {
      $items = [];
      foreach ($entity->getSecurityUpdates() as $key => $value) {
        $items[] = $key . ': ' . $value;
      }
      $updates_list[] = [
        '#type' => 'details',
        '#title' => $this->t('@amount security updates', ['@amount' => count($items)]),
        '#description' => [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $items,
        ],
        '#attributes' => ['class' => ['available-security-updates']],
        '#open' => FALSE,
      ];
    }
    else {
      $updates_list[] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No security updates'),
        '#attributes' => ['class' => ['last-update']],
      ];
    }

    if ($entity->hasFeatureUpdates()) {
      $items = [];
      foreach ($entity->getFeatureUpdates() as $key => $value) {
        $items[] = $key . ': ' . $value;
      }
      $updates_list[] = [
        '#type' => 'details',
        '#title' => $this->t('@amount feature updates', ['@amount' => count($items)]),
        '#description' => [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $items,
        ],
        '#attributes' => ['class' => ['available-feature-updates']],
        '#open' => FALSE,
      ];
    }
    else {
      $updates_list[] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No feature updates'),
        '#attributes' => ['class' => ['last-update']],
      ];
    }

    return $updates_list;
  }

}
