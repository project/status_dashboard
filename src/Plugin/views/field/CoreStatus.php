<?php

namespace Drupal\status_dashboard\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Project Core Status field handler.
 *
 * @ViewsField("status_dashboard_project_core_status")
 */
class CoreStatus extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    if (!$entity) {
      return [];
    }
    $security_updates = $entity->hasSecurityUpdates() ? $entity->getSecurityUpdates() : [];
    $feature_updates = $entity->hasFeatureUpdates() ? $entity->getFeatureUpdates() : [];
    $status = 'last-update';
    if (!empty($feature_updates['Drupal core'])) {
      $status = 'available-feature-updates';
    }
    if (!empty($security_updates['Drupal core'])) {
      $status = 'available-security-updates';
    }

    return [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $entity->getCoreStatus(),
      '#attributes' => [
        'class' => [$status],
      ],
    ];
  }

}
