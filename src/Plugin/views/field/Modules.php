<?php

namespace Drupal\status_dashboard\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Project Modules field handler.
 *
 * @ViewsField("status_dashboard_project_modules")
 */
class Modules extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    if (!$entity) {
      return [];
    }
    $module_list = [];

    if ($entity->hasModules()) {
      $items = [];
      foreach ($entity->getModules() as $list) {
        foreach ($list as $key => $value) {
          $items[] = $key . ': ' . $value;
        }
      }
      $module_list[] = [
        '#type' => 'details',
        '#title' => $this->t('@amount modules', ['@amount' => count($items)]),
        '#description' => [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $items,
        ],
        '#attributes' => ['class' => ['module-list']],
        '#open' => FALSE,
      ];
    }

    return $module_list;
  }

}
