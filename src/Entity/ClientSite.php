<?php

namespace Drupal\status_dashboard\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\status_dashboard\ClientSiteInterface;

/**
 * Defines the client site entity class.
 *
 * @ContentEntityType(
 *   id = "client_site",
 *   label = @Translation("Client site"),
 *   label_collection = @Translation("Client sites"),
 *   label_singular = @Translation("Client site"),
 *   label_plural = @Translation("Client sites"),
 *   label_count = @PluralTranslation(
 *     singular = "@count client site",
 *     plural = "@count client sites",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\status_dashboard\Form\ClientSiteForm",
 *       "edit" = "Drupal\status_dashboard\Form\ClientSiteForm",
 *       "delete" = "Drupal\status_dashboard\Form\ClientSiteDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "status_dashboard_client_site",
 *   admin_permission = "administer status dashboard configuration",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "canonical" = "/admin/client-site/{client_site}",
 *     "add-form" = "/admin/client-site/add",
 *     "edit-form" = "/admin/client-site/{client_site}/edit",
 *     "delete-form" = "/admin/client-site/{client_site}/delete"
 *   },
 * )
 */
class ClientSite extends ContentEntityBase implements ClientSiteInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function hasSecurityUpdates(): bool {
    return !$this->get('security_updates')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function hasFeatureUpdates(): bool {
    return !$this->get('feature_updates')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function hasStatusErrors(): bool {
    if (!$this->get('status_errors')->isEmpty()) {
      $value = $this->get('status_errors')->getString();
      if ($value != 0 && $value != NULL) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasModules(): bool {
    return !$this->get('modules')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getSecurityUpdates() {
    return $this->get('security_updates')->first()->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setSecurityUpdates(array $security_updates): ClientSite {
    $this->set('security_updates', $security_updates);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFeatureUpdates() {
    return $this->get('feature_updates')->first()->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setFeatureUpdates(array $feature_updates): ClientSite {
    $this->set('feature_updates', $feature_updates);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusErrors(): string {
    if ($this->hasStatusErrors()) {
      return $this->get('status_errors')->getString();
    }
    return "";
  }

  /**
   * {@inheritdoc}
   */
  public function setStatusErrors(int $status_errors): ClientSite {
    $this->set('status_errors', $status_errors);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->get('id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setId(string $id): ClientSite {
    $this->set('id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSitename() {
    return $this->get('sitename')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSitename(string $sitename): ClientSite {
    $this->set('sitename', $sitename);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return $this->get('url')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUrl(string $url): ClientSite {
    $this->set('url', $url);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    return $this->get('tags')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function getTagsName(): array {
    $terms = [];
    foreach ($this->getTags() as $tag) {
      $terms[] = $tag->getName();
    }
    return $terms;
  }

  /**
   * {@inheritdoc}
   */
  public function setTags(array $tags): ClientSite {
    $this->set('tags', $tags);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCoreStatus() {
    return $this->get('core_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCoreStatus(string $core_status): ClientSite {
    $this->set('core_status', $core_status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getModules() {
    return $this->get('modules')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setModules(array $modules): ClientSite {
    $this->set('modules', $modules);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastChecked() {
    return $this->get('last_checked')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastChecked(int $last_checked): ClientSite {
    $this->set('last_checked', $last_checked);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of client web site.'))
      ->setReadOnly(TRUE);

    $fields['sitename'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Site name'))
      ->setDescription(t('Client web site name.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 100,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['url'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('URL'))
      ->setDescription(t('Client web site url.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 100,
      ]);

    $fields['core_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Core status'))
      ->setDescription(t('Drupal version.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 100,
      ]);

    $fields['modules'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Modules'))
      ->setDescription(t('Drupal modules version.'));

    $fields['security_updates'] = BaseFieldDefinition::create('map')
      ->setLabel(t('security updates'))
      ->setDescription(t('Drupal security updates.'));

    $fields['status_errors'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('status errors'))
      ->setDescription(t('Total number of status errors'))
      ->setDefaultValue(0);

    $fields['has_security_updates'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Has security updates'))
      ->setDefaultValue(FALSE)
      ->setReadOnly(TRUE);

    $fields['feature_updates'] = BaseFieldDefinition::create('map')
      ->setLabel(t('feature updates'))
      ->setDescription(t('Drupal feature updates.'));

    $fields['last_checked'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last checked'))
      ->setDescription(t('The time that the Drupal version was checked.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ]);

    $fields['tags'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Dashboard tags'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings',
        [
          'target_bundles' => [
            'dashboard_tags' => 'dashboard_tags',
          ],
        ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete_tags',
        'weight' => 3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '10',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $this->set('has_security_updates', $this->hasSecurityUpdates());
  }

}
