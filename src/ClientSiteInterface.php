<?php

namespace Drupal\status_dashboard;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a client site entity type.
 */
interface ClientSiteInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets whether the client site has security updates.
   */
  public function hasSecurityUpdates();

  /**
   * Gets whether the client site has feature updates.
   */
  public function hasFeatureUpdates();

  /**
   * Gets whether the client site has status errors.
   */
  public function hasStatusErrors();

  /**
   * Gets whether the client site has modules.
   */
  public function hasModules();

  /**
   * Gets the security updates.
   */
  public function getSecurityUpdates();

  /**
   * Sets the security updates.
   */
  public function setSecurityUpdates(array $security_updates);

  /**
   * Gets the feature updates.
   */
  public function getFeatureUpdates();

  /**
   * Sets the feature updates.
   */
  public function setFeatureUpdates(array $feature_updates);

  /**
   * Gets the status errors count at client site.
   */
  public function getStatusErrors();

  /**
   * Sets the status errors count at client site.
   *
   * @param int $status_errors
   *   The status errors count.
   */
  public function setStatusErrors(int $status_errors);

  /**
   * Gets the id.
   */
  public function getId();

  /**
   * Sets the id.
   */
  public function setId(string $id);

  /**
   * Gets the sitename.
   */
  public function getSitename();

  /**
   * Sets the sitename.
   */
  public function setSitename(string $sitename);

  /**
   * Gets the url.
   */
  public function getUrl();

  /**
   * Sets the url.
   */
  public function setUrl(string $url);

  /**
   * Gets the tags.
   */
  public function getTags();

  /**
   * Gets the tags.
   */
  public function getTagsName();

  /**
   * Sets the tags.
   */
  public function setTags(array $tags);

  /**
   * Gets the core status.
   */
  public function getCoreStatus();

  /**
   * Sets the core status.
   */
  public function setCoreStatus(string $core_status);

  /**
   * Gets the modules.
   */
  public function getModules();

  /**
   * Sets the modules.
   */
  public function setModules(array $modules);

  /**
   * Gets the last check.
   */
  public function getLastChecked();

  /**
   * Sets the last check.
   */
  public function setLastChecked(int $last_checked);

}
