<?php

namespace Drupal\status_dashboard;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Collector service to collect the updates info from the client site.
 */
class Collector implements CollectorInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Collector object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->logger = $logger->get('status_dashboard');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function collect(string $url, string $secrete) {
    $url = rtrim(trim($url), "\\/");
    try {
      $options['headers'] = ['x-dashboard-secret' => $secrete];
      $response = $this->httpClient->request(
        'GET',
        sprintf('%s/status_dashboard/check', $url),
        $options
      );
      if ($response->getStatusCode() !== 200) {
        $this->logger->error('Invalid connection with client site');
      }
      else {
        $json = $response->getBody()->getContents();
        $json = preg_replace('/[\x{200B}-\x{200D}\x{FEFF}]/u', '', $json);
        $values = json_decode($json, TRUE);
        if (is_null($values)) {
          $msg = json_last_error_msg();
          $this->logger->error("JSON cannot be decoded: $msg");
          return;
        }

        $storage = $this->entityTypeManager->getStorage('client_site');

        $query = $storage->getQuery()
          ->accessCheck(FALSE)
          ->condition('url', $url);
        $ids = $query->execute();

        $id = reset($ids);
        /** @var \Drupal\status_dashboard\Entity\ClientSite $site_status */
        if ($id) {
          $site_status = $storage->load($id);
          $site_status->setCoreStatus($values['core']);
          $site_status->setModules($values['modules']);
          $site_status->setSecurityUpdates($values['security_updates']);
          $site_status->setFeatureUpdates($values['feature_updates']);
          // Check if client is reporting error_count
          // as it might not have this ability.
          if (array_key_exists('error_count', $values)) {
            $site_status->setStatusErrors($values['error_count']);
          }
          $site_status->setLastChecked($values['date']);
        }
        else {
          $site_status = $storage->create([
            'sitename' => $values['sitename'],
            'core_status' => $values['core'],
            'url' => $url,
            'modules' => $values['modules'],
            'security_updates' => $values['security_updates'],
            'feature_updates' => $values['feature_updates'],
            'status_errors' => $values['error_count'],
            'last_checked' => $values['date'],
          ]);
        }

        $site_status->save();
      }
    }
    catch (GuzzleException $e) {
      $this->logger->error($e->getMessage());
    }
  }

}
