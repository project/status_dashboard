<?php

namespace Drupal\status_dashboard\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Client Site edit form.
 */
class ClientSiteForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result === SAVED_NEW
      ? $this->t('%label has been created.', $message_args)
      : $this->t('%label has been updated.', $message_args);
    $this->messenger()->addStatus($message);
    return $result;
  }

}
