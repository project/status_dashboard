<?php

namespace Drupal\status_dashboard\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\status_dashboard\Entity\ClientSite;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Status dashboard form.
 */
class DashboardForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The request query parameters.
   *
   * @var array
   */
  protected $requestParams;

  /**
   * DashboardForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   Database Service Object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->requestParams = $this->getRequest()->query->all();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'status_dashboard_dashboard';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'status_dashboard/status_dashboard';

    $this->attachFilter($form);

    $form['dashboard'] = [
      '#type' => 'table',
      '#title' => $this->t('Dashboard'),
      '#header' => $this->getHeader(),
      '#sticky' => TRUE,
      '#required' => TRUE,
    ];

    if ($this->requestParams) {
      $results = $this->getSitenamesResults($this->requestParams);
    }
    else {
      $results = $this->entityTypeManager
        ->getStorage('client_site')->loadMultiple();
    }
    if ($results) {
      usort($results, fn($a, $b) => strcmp($a->getSitename(), $b->getSitename()));
      foreach ($results as $result) {
        $this->createRow($form, $result);
      }
    }

    return $form;
  }

  /**
   * Creates a table row.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\status_dashboard\Entity\ClientSite $entity
   *   The row data.
   */
  private function createRow(array &$form, ClientSite $entity): void {
    $security_updates = $entity->hasSecurityUpdates() ? $entity->getSecurityUpdates() : NULL;
    $feature_updates = $entity->hasFeatureUpdates() ? $entity->getFeatureUpdates() : NULL;
    $modules = $entity->hasModules() ? $entity->getModules() : NULL;
    $status_errors = $entity->hasStatusErrors() ? $entity->getStatusErrors() : NULL;
    $url = $entity->getUrl() ? Url::fromUri($entity->getUrl()) : NULL;

    $statusColor = 'last-update';
    if (!empty($feature_updates['Drupal core'])) {
      $statusColor = 'available-feature-updates';
    }
    if (!empty($security_updates['Drupal core'])) {
      $statusColor = 'available-security-updates';
    }
    $hasStatusErrorsColor = '';
    if (!empty($status_errors)) {
      $hasStatusErrorsColor = 'available-security-updates';
    }

    $core_status = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $entity->getCoreStatus(),
      '#attributes' => [
        'class' => [$statusColor],
      ],
    ];
    $status_errors = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $entity->getStatusErrors(),
      '#attributes' => ['class' => [$hasStatusErrorsColor]],
    ];

    if ($url !== NULL) {
      $client_url = [
        '#title' => $entity->getUrl(),
        '#type' => 'link',
        '#url' => $url,
        '#attributes' => [
          'target' => '_blank',
        ],
      ];
    }
    else {
      $client_url = [
        '#markup' => $this->t('Note: Run CRON to get last updates.'),
      ];
    }

    $module_list = [];

    if ($modules !== NULL) {
      $items = [];
      foreach ($modules as $list) {
        foreach ($list as $key => $value) {
          $items[] = $key . ': ' . $value;
        }
      }
      $module_list[] = [
        '#type' => 'details',
        '#title' => $this->t('@amount modules', ['@amount' => count($items)]),
        '#description' => [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $items,
        ],
        '#attributes' => ['class' => ['module-list']],
        '#open' => FALSE,
      ];
    }

    $updates_list = [];

    if ($security_updates !== NULL) {
      $items = [];
      foreach ($security_updates as $key => $value) {
        $items[] = $key . ': ' . $value;
      }
      $updates_list[] = [
        '#type' => 'details',
        '#title' => $this->t('@amount security updates', ['@amount' => count($items)]),
        '#description' => [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $items,
        ],
        '#attributes' => ['class' => ['available-security-updates']],
        '#open' => FALSE,
      ];
    }
    else {
      $updates_list[] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No security updates'),
        '#attributes' => ['class' => ['last-update']],
      ];
    }

    if ($feature_updates !== NULL) {
      $items = [];
      foreach ($feature_updates as $key => $value) {
        $items[] = $key . ': ' . $value;
      }
      $updates_list[] = [
        '#type' => 'details',
        '#title' => $this->t('@amount feature updates', ['@amount' => count($items)]),
        '#description' => [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $items,
        ],
        '#attributes' => ['class' => ['available-feature-updates']],
        '#open' => FALSE,
      ];
    }
    else {
      $updates_list[] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No feature updates'),
        '#attributes' => ['class' => ['last-update']],
      ];
    }

    $edit_form = [
      '#type' => 'link',
      '#title' => $this->t('Edit'),
      "#weight" => 1,
      '#url' => Url::fromRoute('entity.client_site.edit_form', ['client_site' => $entity->getId()]),
      '#attributes' => ['target' => '_blank', 'class' => 'button'],
    ];

    $sitename = [
      '#type' => 'container',
    ];

    $sitename['sitename'] = [
      '#type' => 'processed_text',
      '#text' => $entity->getSitename(),
      '#format' => 'full_html',
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    ];
    $sitename['tags'] = [];
    foreach ($entity->getTagsName() as $name) {
      $sitename['tags'][] = [
        '#type' => 'processed_text',
        '#text' => $name,
        '#format' => 'full_html',
        '#prefix' => '<span class="dashboard-tags">',
        '#suffix' => '</span> &nbsp;',
      ];
    }

    $form['dashboard']['#rows'][] = [
      ['data' => $sitename],
      ['data' => $client_url],
      ['data' => $module_list],
      ['data' => $core_status],
      ['data' => $updates_list],
      ['data' => $status_errors],
      ['data' => date('d-m-Y H:i', $entity->getLastChecked())],
      ['data' => $edit_form],
    ];
  }

  /**
   * Gets the table header for the status dashboard.
   *
   * @return array[]
   *   Table header for the status dashboard.
   */
  public function getHeader(): array {
    return [
      $this->t('Sitename'),
      $this->t('URL'),
      $this->t('Modules'),
      $this->t('Core status'),
      $this->t('Modules updates'),
      $this->t('Status Errors'),
      $this->t('Last checked'),
      $this->t('Edit form'),
    ];
  }

  /**
   * Gets sitenames query results.
   *
   * @param array $parameters
   *   The filter parameters.
   *
   * @return array
   *   Query results.
   */
  private function getSitenamesResults(array $parameters = []): array {
    /** @var \Drupal\Core\Database\Query\SelectInterface $query */
    $query = $this->database->select('status_dashboard_client_site', 'sd');

    $query->addField('sd', 'id');

    if (isset($parameters['search'])) {
      $query->condition('sd.sitename', '%' . $this->database->escapeLike($parameters['search']) . '%', 'LIKE');
    }
    $query_res = $query->execute();
    $rows = [];
    while ($row = $query_res->fetchAssoc()) {
      $rows[] = $row['id'];
    }

    if ($rows) {
      return $this->entityTypeManager
        ->getStorage('client_site')->loadMultiple($rows);
    }

    return [];
  }

  /**
   * Attaches the filters to the form.
   */
  private function attachFilter(array &$form): void {
    $params = $this->getRequest()->query;

    $form['filter'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['filter-container'],
      ],
    ];

    $form['filter']['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#default_value' => $params->get('search'),
    ];

    $form['filter']['actions'] = [
      '#type' => 'actions',
    ];
    $form['filter']['actions']['apply_filter'] = [
      '#type' => 'submit',
      '#name' => 'filter',
      '#value' => $this->t('Apply'),
      '#validate' => [],
      '#submit' => [[$this, 'applyFilter']],
      '#limit_validation_errors' => [
        ['search'],
      ],
    ];
  }

  /**
   * Filters apply handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function applyFilter(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $options = [];
    foreach ($values as $key => $value) {
      if ($key !== 'apply_filter' && $key !== 'filter' && (isset($value) && $value !== '')) {
        $options['query'][$key] = $value;
      }
    }

    $form_state->setRedirect($this->getRouteMatch()
      ->getRouteName(), $this->getRouteMatch()
      ->getParameters()
      ->all(), $options);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Generic submit is not used.
  }

}
