<?php

namespace Drupal\status_dashboard\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure status dashboards settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'status_dashboard_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'status_dashboard.settings',
      'status_dashboard.mail_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['url_and_secret'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('URL and Secret'),
      '#prefix' => '<div id="url-and-secret">',
      '#suffix' => '</div>',
    ];
    $saved_values = $this->getSavedValues();
    $tempo_values = $this->getRowValuesFromUserInput($form_state);
    $prefill_values = [];
    if (!empty($saved_values) && empty($form_state->getUserInput())) {
      $prefill_values = $saved_values;
    }
    if (empty($prefill_values) && !empty($form_state->getUserInput())) {
      $prefill_values = $tempo_values;
    }
    if (!empty($prefill_values['enable_email_notifications']) && ($prefill_values['email']) && ($prefill_values['period']) && ($prefill_values['notification_types'])) {
      unset($prefill_values['enable_email_notifications']);
      unset($prefill_values['email']);
      unset($prefill_values['period']);
      unset($prefill_values['notification_types']);
    }
    $i = 0;
    foreach ($prefill_values as $prefill_value) {
      $form['url_and_secret']['row_' . $i] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'row_' . $i,
          'name' => 'row_' . $i,
          'class' => ['url-row-wrapper'],
        ],

      ];

      $form['url_and_secret']['row_' . $i]['url'] = [
        '#type' => 'url',
        '#title' => $this->t('URL'),
        '#description' => $this->t('URL of client site.'),
        '#attributes' => [
          'id' => 'url_' . $i,
          'name' => 'url_' . $i,
        ],
      ];
      $form['url_and_secret']['row_' . $i]['secret'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Secret'),
        '#description' => $this->t('Secret sign for connection with client site.'),
        '#attributes' => [
          'id' => 'secret_' . $i,
          'name' => 'secret_' . $i,
        ],
      ];
      $form['url_and_secret']['row_' . $i]['remove_row_' . $i] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => ['::removeProtectionCode'],
        '#ajax' => [
          'callback' => '::alterPageAndCodesSettingsRowsCallback',
          'wrapper' => 'url-and-secret',
        ],
        '#name' => 'remove_row_' . $i,
        '#attributes' => [
          'name' => 'remove_btn_' . $i,
          'class' => ['remove-button'],
        ],
      ];

      if (array_key_exists($i, $prefill_values)) {
        $form['url_and_secret']['row_' . $i]['url']['#default_value'] = $prefill_value['url'];
        $form['url_and_secret']['row_' . $i]['secret']['#default_value'] =
          $prefill_value['secret'] ?? '';
      }
      $i++;
    }
    $form['url_and_secret']['add_row'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['add-row'],
      ],

    ];
    $form['url_and_secret']['add_row']['button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add url and secret'),
      '#submit' => ['::addProtectionCode'],
      '#ajax' => [
        'callback' => '::alterPageAndCodesSettingsRowsCallback',
        'wrapper' => 'url-and-secret',
      ],
      '#attributes' => [
        'class' => ['add-row-button'],
      ],
    ];

    $form['enable_email_notifications'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable email notifications'),
      '#default_value' => $this->config('status_dashboard.mail_settings')
        ->get('enable_email_notifications'),
    ];
    $form['email_notifications'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Email notifications'),
      '#states' => [
        'visible' => [
          'input[name="enable_email_notifications"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['email_notifications']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Email address for notifications of updates in client sites.'),
      '#default_value' => $this->config('status_dashboard.mail_settings')
        ->get('email'),
      '#states' => [
        'required' => [
          'input[name="enable_email_notifications"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['email_notifications']['period'] = [
      '#type' => 'select',
      '#title' => $this->t('Period'),
      '#description' => $this->t('Notification frequency.'),
      '#options' => [
        'daily' => $this->t('Daily'),
        'weekly' => $this->t('Weekly'),
        'monthly' => $this->t('Monthly'),
      ],
      '#default_value' => $this->config('status_dashboard.mail_settings')
        ->get('period'),
    ];
    $form['email_notifications']['notification_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Notification types'),
      '#description' => $this->t('Select the notifications you want to receive on the selected email:
        1. Core updates - notification only about Drupal core update.
        2. Security updates - notification about security updates of Drupal core and contrib modules.
        3. Feature updates - notification about feature updates of the Drupal core and contrib modules.'),
      '#default_value' => $this->config('status_dashboard.mail_settings')->get('notification_types') ?? [],
      '#options' => [
        'type1' => $this->t('Core updates'),
        'type2' => $this->t('Security updates'),
        'type3' => $this->t('Feature updates'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the settings form with the existing rows in it.
   */
  public function alterPageAndCodesSettingsRowsCallback(array &$form, FormStateInterface $form_state) {
    return $form['url_and_secret'];
  }

  /**
   * Retrieve saved in config settings for protected pages.
   */
  protected function getSavedValues() {
    return $this->config('status_dashboard.settings')->getRawData();
  }

  /**
   * Retrieve temporary settings for protected pages.
   */
  protected function getRowValuesFromUserInput(FormStateInterface $form_state) {
    $tempo_values = $form_state->getUserInput();
    $result = [];
    foreach ($tempo_values as $key => $value) {
      if (str_starts_with($key, 'url_') && !str_starts_with($key, 'secret')) {
        $node_id = filter_var($value, FILTER_VALIDATE_URL);
        $row_id = (int) filter_var($key, FILTER_SANITIZE_NUMBER_INT);
        $secret = '';
        if (array_key_exists('secret_' . $row_id, $tempo_values)) {
          $secret = $tempo_values['secret_' . $row_id];
        }
        $result[] = [
          'url' => $node_id,
          'secret' => $secret,
        ];
      }
    }
    return $result;
  }

  /**
   * Adds new url-protection code combination input fields to a form.
   */
  public function addProtectionCode(array &$form, FormStateInterface $form_state) {
    // Increase row counter.
    $array_keys = array_keys($form['url_and_secret']);
    $rows_num = -1;
    foreach ($array_keys as $key) {
      if (str_starts_with($key, 'row_')) {
        $rows_num++;
      }
    }
    $user_input = $form_state->getUserInput();
    $user_input['url_' . ($rows_num + 1)] = '';
    $user_input['secret_' . ($rows_num + 1)] = '';
    $form_state->setUserInput($user_input);
    $form_state->setRebuild();
  }

  /**
   * Remove handler for specific protection url-code combination.
   *
   * Removes triggered combination from form_state.
   */
  public function removeProtectionCode(array &$form, FormStateInterface $form_state) {
    $triggerer_name = end($form_state->getTriggeringElement()['#parents']);
    $triggerer_id = (int) filter_var($triggerer_name, FILTER_SANITIZE_NUMBER_INT);
    $user_input = $form_state->getUserInput();
    unset($user_input['url_' . ($triggerer_id)]);
    unset($user_input['secret_' . ($triggerer_id)]);
    $form_state->setUserInput($user_input);

    $form_state->setRebuild();
  }

  /**
   * Notification types field validation.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getValue('enable_email_notifications') === 1) {
      $notification_types = $form_state->getValue('notification_types');
      if (array_filter($notification_types) === []) {
        $form_state->setErrorByName('notification_types', $this->t('At least one notification type should be selected.'));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user_input_for_rows = $this->getRowValuesFromUserInput($form_state);
    // Collecting row data and checking its validity.
    $result = [];
    foreach ($user_input_for_rows as $key => $values) {
      $secret = $values['secret'];
      $url = $values['url'];
      if (!empty($url) && !empty($secret)) {
        $result[$key] = [
          'url' => $url,
          'secret' => $secret,
        ];
      }
    }
    $config = $this->config('status_dashboard.settings');
    $config_mail = $this->config('status_dashboard.mail_settings');
    $config->setData($result);
    $config_mail->set('email', $form_state->getValue('email'))
      ->set('enable_email_notifications', $form_state->getValue('enable_email_notifications'))
      ->set('period', $form_state->getValue('period'))
      ->set('notification_types', $form_state->getValue('notification_types'));
    $config_mail->save();
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
