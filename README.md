# Status Dashboard

The Status Dashboard module is developed to monitor Drupal sites.
The module review available updates for Drupal core version and Drupal
modules and displays them in the dashboard.

Status Dashboard is provides for developers to track information about
updates from Drupal system of multiple sites. This makes it possible to
have all information about updates of the Drupal version and modules of
all Drupal sites on the dashboard.

The module has such functionality as sending a notification to the Email.
In the Email notification settings, you can select the mail address, frequency
of sending and type of notification.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/status_dashboard).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/status_dashboard).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers
- Supporting organizations


## Requirements

On client sites you have to install the supporting module.

- [Status Dashboard Client](https://www.drupal.org/project/status_dashboard_client)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Open configuration form on URL /admin/config/development/status-dashboard
1. Add the data to the URL AND SECRET fieldset which has the property multiple:
   - URL - the name of the client site whose updates you want to monitor
   - Secret - secret data for connecting with client site
1. If you want to receive notifications by mail, then you need to fill the Email
   notifications fieldset by clicking the Enable email notifications checkbox:
   - Email - Email address for sending notifications
   - Period - Notification frequency which consists of three periods (Daily,
     Weekly, Monthly)
   - Notification types - Types of updates (Core updates, Security updates,
     Feature updates) to be sent to the specified email
1. Install on client sites the supporting module Status Dashboard Client
1. Enable the module
1. Open the configuration form on
   /admin/config/development/status-dashboard-client
1. Add data for the Secret field, which is specified in the main site for
   connecting to this site


## Maintainers

- Alexey Sivets - [w.drupal](https://www.drupal.org/u/wdrupal)
- Farid Muborakshoev - [farid.muborakshoev](https://www.drupal.org/u/faridmuborakshoev)
- Bram Driesen - [BramDriesen](https://www.drupal.org/u/bramdriesen)


## Supporting organizations
- [Engine Eight](https://www.drupal.org/engine-eight)
- [Solvy](https://www.drupal.org/solvy)
