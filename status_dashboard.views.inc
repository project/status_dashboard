<?php

/**
 * @file
 * Provide views data for status_dashboard.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function status_dashboard_views_data_alter(array &$data) {
  $data['status_dashboard_client_site']['project_updates'] = [
    'title' => t('Core and Module Updates'),
    'group' => t('Client site'),
    'field' => [
      'title' => t('Core and Module Updates'),
      'id' => 'status_dashboard_project_updates',
    ],
  ];
  $data['status_dashboard_client_site']['modules'] = [
    'title' => t('Project Modules'),
    'group' => t('Client site'),
    'field' => [
      'title' => t('Modules List'),
      'id' => 'status_dashboard_project_modules',
    ],
  ];
  $data['status_dashboard_client_site']['core_status'] = [
    'title' => t('Core Status'),
    'group' => t('Client site'),
    'field' => [
      'title' => t('Core Status'),
      'id' => 'status_dashboard_project_core_status',
    ],
  ];
}
